import java.io.UnsupportedEncodingException;

/**
 * Created by denysord88 on 5/21/16.
 */
public class HEXToText {
    public static final String HEX_ANCHOR = "\\x";
    public static String encoding = "windows-1251";

    public static String decodeTextWithSymbolsHexCodes (String textWithHex) throws UnsupportedEncodingException {
        int length = textWithHex.length();
        int i = textWithHex.indexOf(HEX_ANCHOR);
        StringBuilder sb = new StringBuilder();
        int prevI = 0;
        while (i >= 0 && i < length - 3) {
            sb.append(textWithHex.substring(prevI, i));
            String charCode = textWithHex.substring(i + 2, i + 4);
            if (charCode.replaceAll("[abcdefABCDEF\\d]", "").length() == 0) {
                sb.append(decodeSymbolFromHex(charCode));
            } else {
                i = textWithHex.indexOf(HEX_ANCHOR, i + HEX_ANCHOR.length());
                continue;
            }
            prevI = i + 4;
            i = textWithHex.indexOf(HEX_ANCHOR, i + HEX_ANCHOR.length());
        }
        sb.append(textWithHex.substring(prevI));
        return normalizeText (sb.toString());
    }

    private static String decodeSymbolFromHex(String hexCharCode) throws UnsupportedEncodingException {
        hexCharCode = "0x" + hexCharCode;
        char c = (char) Integer.decode(hexCharCode).intValue();
        byte[] charBytes = {(byte) (c & 0xff)};
        //byte[] char2Bytes = {(byte) (c & 0xff), (byte) (c >> 8 & 0xff)};
        return new String(charBytes, encoding);
    }

    private static String normalizeText (String text) {
        return text.replaceAll("&nbsp;", " ").replaceAll("&nbsp", " ");
    }
}
