/**
 * Created by denysord88 on 5/21/16.
 */

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class GUI extends Application {
    private Stage window;
    public static String titleText = "Hex codes to text decoder v1.0 - http://murrzik.su/deco/";

    // Инициализация приложения
    public void init(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Label encodedLabel = new Label("Введите закодированный текст\nс символами вида \\xFF");
        TextArea encodedArea = new TextArea();
        encodedArea.setPrefRowCount(19);
        encodedArea.setPrefWidth(2500);
        encodedArea.setPrefColumnCount(100);
        encodedArea.setWrapText(true);
        encodedArea.setEditable(true);
        Button decodeButton = new Button("Раскодировать --->");
        decodeButton.setMinWidth(200);
        VBox encodedLayout = new VBox(3);
        encodedLayout.setAlignment(Pos.TOP_LEFT);
        encodedLayout.getChildren().addAll(encodedLabel, encodedArea, decodeButton);

        Label decodedLabel = new Label("Раскодированный текст:\n_");
        TextArea decodedArea = new TextArea();
        decodedArea.setPrefRowCount(19);
        decodedArea.setPrefWidth(2500);
        decodedArea.setPrefColumnCount(100);
        decodedArea.setWrapText(true);
        decodedArea.setEditable(true);
        ObservableList<String> encOoptions = FXCollections.observableArrayList(Charset.availableCharsets().keySet());
        ComboBox encComboBox = new ComboBox(encOoptions);
        encComboBox.setEditable(false);
        encComboBox.setValue("windows-1251");
        encComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                HEXToText.encoding = t1;
            }
        });
        VBox decodedLayout = new VBox(3);
        decodedLayout.setAlignment(Pos.TOP_LEFT);
        decodedLayout.getChildren().addAll(decodedLabel, decodedArea, encComboBox);

        decodeButton.setOnAction(e -> {
            try {
                decodedArea.setText(HEXToText.decodeTextWithSymbolsHexCodes(encodedArea.getText()));
            } catch (UnsupportedEncodingException e1) {
                decodedArea.setText(e1.getMessage());
            }
        });

        HBox mainBox = new HBox(2);
        mainBox.getChildren().addAll(encodedLayout, decodedLayout);
        mainBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(mainBox, 700, 450);

        scene.heightProperty().addListener((observableValue, oldSceneHeight, newSceneHeight) -> {
            if (newSceneHeight.doubleValue() > 411.0) {
                encodedArea.setPrefHeight(newSceneHeight.doubleValue() - 108.0);
                decodedArea.setPrefHeight(newSceneHeight.doubleValue() - 108.0);
            }
        });

        scene.getStylesheets().add("css/Evil.css");

        window = primaryStage;
        window.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
        window.setTitle(titleText);
        window.setScene(scene);
        window.setWidth(700);
        window.setHeight(450);
        window.show();
    }
}
